# 1.7.0
- `IFS-262`: `isyfact-masterpom` deprecated (Abschaffung mit IsyFact 2.0), `isyfact-masterpom-lib` aufgelöst, Bibliotheken benutzen `isyfact-standards` als Parent-POM

# 1.6.0
- `IFS-189`: Repositories der IsyFact-Standards zusammengeführt, Bibliotheken benutzen wieder gemeinsames Produkt-BOM und werden zentral über das POM isyfact-standards versioniert

# 1.4.2
- `IFS-95`: Properties in distributionManagement geändert

# 1.4.0
- `IFS-17`: Umbenennung der Artifact-ID und Group-ID, alle plis-Bibliotheken gebannt
